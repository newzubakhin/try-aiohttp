from aiohttp import web


async def handle_404(message):
    return web.json_response(data={'error': message}, status=404)

async def handle_400(message):
    return web.json_response(data={'error': message}, status=400)

async def handle_500(message):
    return web.json_response({'error': message}, status=500)


def create_error_middleware(overrides):
    @web.middleware
    async def error_middleware(request, handler):

        try:
            response = await handler(request)

            override = overrides.get(response.status)
            if override:
                return await override(request)
            return response

        except web.HTTPException as ex:
            override = overrides.get(ex.status)
            message = ex.reason
            if override:
                return await override(message)
            raise

    return error_middleware


def setup_middlewares(app):
    error_middleware = create_error_middleware({
        404: handle_404,
        400: handle_400,
        500: handle_500
    })
    app.middlewares.append(error_middleware)
