import logging
import sys

from aiohttp import web

from try_aiohttp.settings import get_config
from try_aiohttp.routes import setup_routes
from try_aiohttp.middlewares import setup_middlewares

async def init_app(argv=None):

    app = web.Application()

    app['config'] = get_config(argv)

    setup_routes(app)
    setup_middlewares(app)

    return app

def main(argv):
    logging.basicConfig(level=logging.DEBUG)

    app = init_app(argv)

    config = get_config(argv)
    web.run_app(
        app,
        host=config['host'],
        port=config['port']
    )

if __name__ == '__main__':
    main(sys.argv[1:])