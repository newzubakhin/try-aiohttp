import argparse
import pathlib

import trafaret as t
from trafaret_config import commandline

BASE_DIR = pathlib.Path(__file__).parent.parent
CONFIG_PATH = BASE_DIR.joinpath('config/config.yaml')
TRAFARET_CONFIG = t.Dict({
    t.Key('host'): t.IP,
    t.Key('port'): t.Int(),
    t.Key('stride'):
        t.Dict({
            'cloud_id': t.String(),
            'api_url': t.String(),
            'credentials': t.Dict({
                'grant_type': t.String(),
                'client_id': t.String(),
                'client_secret': t.String()
            })
        })
})


def get_config(argv=None):
    ap = argparse.ArgumentParser()
    commandline.standard_argparse_options(ap, default_config=CONFIG_PATH)

    options, _ = ap.parse_known_args(argv)
    config = commandline.config_from_options(options, TRAFARET_CONFIG)
    return config
