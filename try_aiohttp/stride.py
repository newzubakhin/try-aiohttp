from collections import namedtuple
from datetime import datetime, timedelta

import aiohttp

from try_aiohttp.settings import get_config

Token = namedtuple('Token', ['access_token', 'expires_in'])
Response = namedtuple('Response', ['status', 'data'])


class Stride:
    def __init__(self, base_url, cloud_id, credentials):
        self._base_url = base_url
        self._cloud_id = cloud_id
        self._credentials = credentials

    async def send_message(self, conversation_id, message):
        mes_len = len(message)
        if mes_len > 10000:
            reason = 'Message is too long. Limit is 10.000 characters'
            raise aiohttp.web.HTTPBadRequest(reason=reason)
        if mes_len < 1:
            reason = 'Message is too short.'
            raise aiohttp.web.HTTPBadRequest(reason=reason)

        url = '{}/site/{}/conversation/{}/message'.format(
            self._base_url, self._cloud_id, conversation_id)
        headers = await self._get_request_kwargs()

        async with aiohttp.ClientSession() as session:
            async with session.post(
                    url, data=message, headers=headers['headers']) as r:
                data = await r.json()
                status = r.status
                return Response(status=status, data=data)

    async def send_direct_message(self, user_id, message, as_task=False):
        if mes_len > 10000:
            reason = 'Message is too long. Limit is 10.000 characters'
            raise aiohttp.web.HTTPBadRequest(reason=reason)
        if mes_len < 1:
            reason = 'Message is too short.'
            raise aiohttp.web.HTTPBadRequest(reason=reason)

        path = '{}/site/{}/conversation/user/{}/message'.format(
            self._base_url, self._cloud_id, user_id)

        async with aiohttp.ClientSession() as session:
            async with session.post(
                    url, data=message, headers=headers['headers']) as r:
                data = await r.json()
                status = r.status
                return Response(status=status, data=data)

    @property
    async def _get_access_token(self):
        if hasattr(self, '_access_token'):
            token = self._access_token
            expires_in = token.expires_in + timedelta(minutes=5)
            now = datetime.now()

            if expires_in > now:
                return token

        import json
        url = '{}{}'.format(self._base_url, '/oauth/token')
        data = json.dumps(self._credentials)
        headers = {'Content-Type': 'application/json'}
        async with aiohttp.ClientSession() as session:
            async with session.post(url, data=data, headers=headers) as r:
                data = await r.json()
                try:
                    access_token = data['access_token']
                    expire_value = int(data['expires_in'])

                    expires_in = datetime.now() + timedelta(
                        seconds=expire_value)
                    token = Token(access_token, expires_in)
                except (KeyError, ValueError):
                    # raise UnauthorizedException(r.status)
                    raise ValueError

                self._access_token = token

                return token

    async def _get_request_kwargs(self):
        kwargs = {"headers": {'Accept': 'application/json'}}

        token = await self._get_access_token
        kwargs['headers']["Authorization"] = 'Bearer {}'.format(
            token.access_token)
        return kwargs


config = get_config()

stride = Stride(
    cloud_id=config['stride']['cloud_id'],
    credentials=config['stride']['credentials'],
    base_url=config['stride']['api_url'])
