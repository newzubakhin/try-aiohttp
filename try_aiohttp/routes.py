import pathlib

from try_aiohttp.views import index, send_direct_message, send_message


def setup_routes(app):
    app.router.add_get('/', index)
    app.router.add_post(
        path='/message/{conversation_id}',
        handler=send_message,
        name='send_message')
    app.router.add_post(
        path='/direct-message/{user_id}',
        handler=send_direct_message,
        name='send_direct_message')
