from aiohttp import web

from try_aiohttp.stride import stride


async def index(request):
    body = {'message': 'Hello World'}
    return web.json_response(data=body)


async def send_message(request):
    body = await request.json()
    conversation_id = request.match_info['conversation_id']
    try:
        message = body['message']
    except (KeyError, TypeError, ValueError) as e:
        reason = 'Message is not specified.'
        raise web.HTTPBadRequest(reason=reason) from e
    res = await stride.send_message(conversation_id, message)
    return web.json_response(data=res.data, status=res.status)


async def send_direct_message(request):
    body = await request.json()
    user_id = request.match_info['user_id']
    try:
        message = body['message']
    except (KeyError, TypeError, ValueError) as e:
        reason = 'Message is not specified.'
        raise web.HTTPBadRequest(reason=reason) from e
    res = await stride.send_direct_message(user_id, message)
    return web.json_response(data=res.data, status=res.status)
